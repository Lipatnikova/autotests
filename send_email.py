import json
import os
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.base import MIMEBase
from email import encoders
from dotenv import load_dotenv

load_dotenv()


def get_test_results() -> str:
    """Retrieves test results from a JSON file"""
    with open(os.path.join(os.getcwd(), 'public/widgets/suites.json')) as f:
        data = json.load(f)
        stats = data['items'][0]['statistic']
        failed = stats['failed']
        passed = stats['passed']
        return f'Test results: {passed} passed, {failed} failed'


def create_email(message: str, attachment_path: str, from_addr: str, to_addr: str, subject: str) -> MIMEMultipart:
    """
    Creates an email message with an attachment.

    Args:
        message (str): The message content.
        attachment_path (str): The file path of the attachment.
        from_addr (str): The sender's email address.
        to_addr (str): The recipient's email address.
        subject (str): The subject of the email.

    Returns:
        MIMEMultipart: The email message with the attachment.
    """
    msg = MIMEMultipart()
    msg['From'] = from_addr
    msg['To'] = to_addr
    msg['Subject'] = subject

    msg.attach(MIMEText(message, 'plain'))

    with open(attachment_path, "rb") as attachment:
        part = MIMEBase('application', 'octet-stream')
        part.set_payload(attachment.read())
        encoders.encode_base64(part)
        part.add_header('Content-Disposition', f'attachment; filename={os.path.basename(attachment_path)}')
        msg.attach(part)

    return msg


def send_email(msg: MIMEMultipart, smtp_server: str, port: int, username: str, password: str) -> None:
    """
    Sends an email message using SMTP.

    Args:
        msg (MIMEMultipart): The email message to send.
        smtp_server (str): The SMTP server address.
        port (int): The port number for the SMTP server.
        username (str): The username for authentication.
        password (str): The password for authentication.

    Returns:
        None
    """
    server = smtplib.SMTP_SSL(smtp_server, port)
    server.login(username, password)
    server.send_message(msg)
    server.quit()


if __name__ == "__main__":
    YANDEX_EMAIL = os.getenv('YANDEX_EMAIL')
    YANDEX_PASSWORD = os.getenv('YANDEX_PASSWORD')
    MY_EMAIL = os.getenv('MY_EMAIL')
    URL_ALLURE = os.getenv('URL_ALLURE')

    test_results = (f'Here is the link to view the test report: {URL_ALLURE} . \n'
                    f'{get_test_results()} .')

    email_msg = create_email(
        test_results, 'public.zip', YANDEX_EMAIL, MY_EMAIL, 'Test results and Allure report'
    )
    send_email(email_msg, 'smtp.yandex.ru', 465, YANDEX_EMAIL, YANDEX_PASSWORD)
